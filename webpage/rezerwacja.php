<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tatry | Rezerwacja</title>    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link rel="stylesheet" href="./assets/css/font-awesome.min.css">
    
</head>
<body>
    <header>
        <div class="logo"><img src="assets/img/logo.JPG" alt=""> </div>     
            <nav>
                <ul>
                    <li><a href="./index.html">Strona główna</a></li>   
                    <li><a href="./najSzlaki.html">Najsłynniejsze miejsca</a></li> 
                    <li><a href="./galeria.html">Galeria</a></li>
                    <li><a href="./nocleg.html">Zarezerwuj nocleg</a></li>
                </ul>                           
            </nav>
    </header>
    <div class="banner"></div>
    <section class="content">
         <div class="container">
            <form action="http://localhost:85/www/zamowienie.php"method="post">
					<h1>
					<?php
					if($_GET['hotel']==1){
						echo 'Hotel "Marysienka"';}
						
					if($_GET['hotel']==2){
						echo 'Hotel "Pod Dębem"';}
						
					if($_GET['hotel']==3){
						echo 'Hotel Sosenka';}		
					
					?>
					</h1>
                    <select name="pokoje" >
                        <option name="2os">pokój dwuosobowy</option>
                        <option name="3os">pokój dla trzech osób</option>
                    </select>
                    <br>
                    <p>Cena za dobę: </p>
                    <label name="cena">
					<?php
					if($_GET['hotel']==1){
						echo 145 ." zł";}
						
					if($_GET['hotel']==2){
						echo 150 ." zł";}
						
					if($_GET['hotel']==3){
						echo 145 ." zł";}		
					
					?>
					</label>
                    <br>
                    <p>                      
                        chcę dodatkowe łóżko w pokoju
                        <input type="checkbox" name="addBed">
                        <br>                       
                    </p>
                                        
                <p>Imie:</p>
                <input type="text" name="name" size="15" maxlength="20" required>
				<div style="clear:both;"></div>
                <br>
                    <p>Nazwisko:</p>
                <input type="text" name="surname" size="15" maxlength="20" required>
                <br>
                    <p>Telefon:</p>
                <input type="text" name="phone"  placeholder = "Numer telefonu (XXX-XXX-XXX) *" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}"  required>
                <br>
                <br>
                <label for="user">Data zameldowania</label>
                <input type="date"name="przybycie">
                <br>
                    <br>
                    <label for="user">Data wymeldowania</label>
                <input type="date"name="oddanie">
                <br>
                <br>
                <input type="submit" value="Wyślij">

            </form>
        </div>
    </section>
    


    <footer>
         <div class="container">
                <p>Copyright &copy; 2017</p>
            </div>
    </footer>
</body>
</html>